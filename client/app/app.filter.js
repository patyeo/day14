(function () {
    angular.module("EMSApp").filter('customGender', function(){
        return function(input){
            if(input =='M'){
                return "Male";
            }else{
                return "Female";
            }
        }
    });
})();
