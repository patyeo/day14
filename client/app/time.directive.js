// directive can only call on angular filters. dont need to create, just construct in.
// don't bring in the understanding of controller into directive
// rules of angular, dont need to add $ in the scope
// angular is a framework and not a language

(function () {
  angular.module('EMSApp')
      .directive("myCurrentTime", myCurrentTime);
//the directive is name in camelCase. but when being call on in html, it need to be lowercase and with a dash. directive requirement
          function myCurrentTime(dateFilter){
          return function(scope, element, attrs){
              var format;
  
              scope.$watch(attrs.myCurrentTime, function(value) {
//Add a comment to this line
                  format = value;
                  updateTime();
              });
  
              function updateTime(){
                  var dt = dateFilter(new Date(), format);
                  element.text(dt);
              }
  
              function updateLater() {
                  setTimeout(function() {
                      updateTime(); // update DOM
                      updateLater(); // schedule another update
                  }, 1000);
              }
  
              updateLater();
      }
  }
}());

