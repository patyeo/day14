(function(){
    angular
        .module("EMSApp")
        .service("EMSAppAPI", [
            '$http',
            EMSAppAPI
        ]);
    
    function EMSAppAPI($http){
        var self = this;

        /* query string '?string='
        self.searchEmployees = function(value){
            return $http.get("/api/employees?keyword=" + value);
        }*/
        //to query more than 1 item
        self.searchEmployees = function(value, sortby, itemsPerPage, currentPage){
            return $http.get(`/api/employees?keyword=${value}&sortby=${sortby}&itemsPerPage=${itemsPerPage}&currentPage=${currentPage}`);
        }

        // param request
        self.getEmployee = function(emp_no){
            console.log(emp_no);
            return $http.get("/api/employees/" + emp_no)
        }

        // body request
        self.updateEmployee = function(employee){
            console.log(employee);
            return $http.put("/api/employees",employee);
        }


        // parameterized values
        /*
        self.searchEmployees = function(value){
            return $http.get("/api/employees/" + value);
        }*/

        // must learn and master this
//query and params use 'get'
//body use 'post'
// in request, there are 3 ways. one is to post from body, one is to get from parameters, one is by query string
        // post by body over request
        self.addEmployee = function(employee){
            return $http.post("/api/employees", employee);
        }
// here is requesting employee = the body of the data

        self.deleteEmployee = function(emp_no){
            console.log(emp_no);
            return $http.delete("/api/employees/"+ emp_no);
        }
    }
/* this is requesting in the form of parameters - the specific cell. 
be very clear if there is a / to specify the root folder/file to make changes to*/
})();

