/*
* client side

what is inside [] is the dependency that is to be injected into this app, and this dependency could
be an outside library or something another member of team created. If more than one dependency, just
add comma
*/

(function () {
    "use strict";
    angular.module("EMSApp", ['ui.bootstrap']);
})();
