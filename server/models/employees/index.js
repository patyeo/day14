// this is to connect the sequelize function to the database
module.exports = function(connection, Sequelize){
    // employees this have to match the mysql table
    // return Employees object is used within the JS
    var Employees = connection.define('employees', {
        emp_no: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        birth_date: {
            type: Sequelize.DATE,
            allowNull: false
        },
        first_name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        last_name:{
            type: Sequelize.STRING,
            allowNull: false
        },
        gender:{
            type: Sequelize.ENUM('M', 'F'),
            allowNull: false
        },
        hire_date: {
            type: Sequelize.DATE,
            allowNull: false
        },
        photourl: {
            type: Sequelize.STRING,
            defaulValue: "https://cdn.pixabay.com/photo/2014/04/02/10/35/face-303912_960_720.png"
        }

    }, {
        timestamps: false
    });
    return Employees;
}